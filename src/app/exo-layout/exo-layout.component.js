"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var authentication_service_1 = require("../service/authentication.service");
var ExoLayoutComponent = /** @class */ (function () {
    function ExoLayoutComponent(authServ) {
        this.authServ = authServ;
    }
    ExoLayoutComponent.prototype.ngOnInit = function () {
    };
    ExoLayoutComponent.prototype.logged = function () {
        return this.authServ.isLogged();
    };
    ExoLayoutComponent.prototype.logout = function () {
        this.authServ.logout();
    };
    ExoLayoutComponent = __decorate([
        core_1.Component({
            selector: 'ns-exo-layout',
            templateUrl: './exo-layout.component.html',
            styleUrls: ['./exo-layout.component.css'],
            moduleId: module.id,
        }),
        __metadata("design:paramtypes", [authentication_service_1.AuthenticationService])
    ], ExoLayoutComponent);
    return ExoLayoutComponent;
}());
exports.ExoLayoutComponent = ExoLayoutComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXhvLWxheW91dC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJleG8tbGF5b3V0LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUFrRDtBQUNsRCw0RUFBMEU7QUFRMUU7SUFFRSw0QkFBb0IsUUFBOEI7UUFBOUIsYUFBUSxHQUFSLFFBQVEsQ0FBc0I7SUFBSSxDQUFDO0lBRXZELHFDQUFRLEdBQVI7SUFDQSxDQUFDO0lBRUQsbUNBQU0sR0FBTjtRQUNFLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO0lBQ2xDLENBQUM7SUFFRCxtQ0FBTSxHQUFOO1FBQ0UsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEVBQUUsQ0FBQztJQUN6QixDQUFDO0lBYlUsa0JBQWtCO1FBTjlCLGdCQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsZUFBZTtZQUN6QixXQUFXLEVBQUUsNkJBQTZCO1lBQzFDLFNBQVMsRUFBRSxDQUFDLDRCQUE0QixDQUFDO1lBQ3pDLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtTQUNwQixDQUFDO3lDQUc2Qiw4Q0FBcUI7T0FGdkMsa0JBQWtCLENBYzlCO0lBQUQseUJBQUM7Q0FBQSxBQWRELElBY0M7QUFkWSxnREFBa0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQXV0aGVudGljYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZS9hdXRoZW50aWNhdGlvbi5zZXJ2aWNlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnbnMtZXhvLWxheW91dCcsXG4gIHRlbXBsYXRlVXJsOiAnLi9leG8tbGF5b3V0LmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vZXhvLWxheW91dC5jb21wb25lbnQuY3NzJ10sXG4gIG1vZHVsZUlkOiBtb2R1bGUuaWQsXG59KVxuZXhwb3J0IGNsYXNzIEV4b0xheW91dENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSBhdXRoU2VydjpBdXRoZW50aWNhdGlvblNlcnZpY2UpIHsgfVxuXG4gIG5nT25Jbml0KCkge1xuICB9XG5cbiAgbG9nZ2VkKCkge1xuICAgIHJldHVybiB0aGlzLmF1dGhTZXJ2LmlzTG9nZ2VkKCk7XG4gIH1cblxuICBsb2dvdXQoKSB7XG4gICAgdGhpcy5hdXRoU2Vydi5sb2dvdXQoKTtcbiAgfVxufVxuIl19