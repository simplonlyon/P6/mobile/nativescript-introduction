"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var dog_service_1 = require("../service/dog.service");
var image_source_1 = require("tns-core-modules/image-source");
var nativescript_camera_1 = require("nativescript-camera");
var imagepicker = require("nativescript-imagepicker");
var AddDogComponent = /** @class */ (function () {
    function AddDogComponent(dogServ) {
        this.dogServ = dogServ;
        this.dog = { name: '', breed: '' };
    }
    AddDogComponent.prototype.ngOnInit = function () {
    };
    AddDogComponent.prototype.add = function () {
        var _this = this;
        var source = new image_source_1.ImageSource();
        source.fromAsset(this.image).then(function (source) {
            var base64image = source.toBase64String("jpg", 70);
            _this.dog.picture = 'data:image/jpeg;base64,' + base64image;
            _this.dogServ.add(_this.dog).subscribe(function () { return console.log('bravo'); }, function (error) { return console.log(error); });
        });
    };
    AddDogComponent.prototype.takePicture = function () {
        var _this = this;
        var options = {
            width: 300,
            height: 400,
            keepAspectRatio: true,
            saveToGallery: true
        };
        nativescript_camera_1.requestPermissions();
        nativescript_camera_1.takePicture(options).then(function (value) {
            _this.image = value;
        }).catch(function (error) { return console.log(error); });
    };
    AddDogComponent.prototype.pickPicture = function () {
        var _this = this;
        var picker = imagepicker.create({
            mode: 'single'
        });
        picker.authorize()
            .then(function () { return picker.present(); })
            .then(function (image) { return _this.image = image[0]; })
            .catch(function (error) { return console.log(error); });
    };
    AddDogComponent = __decorate([
        core_1.Component({
            selector: 'ns-add-dog',
            templateUrl: './add-dog.component.html',
            styleUrls: ['./add-dog.component.css'],
            moduleId: module.id,
        }),
        __metadata("design:paramtypes", [dog_service_1.DogService])
    ], AddDogComponent);
    return AddDogComponent;
}());
exports.AddDogComponent = AddDogComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWRkLWRvZy5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJhZGQtZG9nLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUFrRDtBQUNsRCxzREFBb0Q7QUFHcEQsOERBQTREO0FBQzVELDJEQUFzRTtBQUV0RSxzREFBd0Q7QUFReEQ7SUFJRSx5QkFBb0IsT0FBbUI7UUFBbkIsWUFBTyxHQUFQLE9BQU8sQ0FBWTtRQUh2QyxRQUFHLEdBQVEsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLEtBQUssRUFBRSxFQUFFLEVBQUUsQ0FBQztJQUdRLENBQUM7SUFFNUMsa0NBQVEsR0FBUjtJQUNBLENBQUM7SUFFRCw2QkFBRyxHQUFIO1FBQUEsaUJBV0M7UUFWQyxJQUFJLE1BQU0sR0FBRyxJQUFJLDBCQUFXLEVBQUUsQ0FBQztRQUM3QixNQUFNLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQyxNQUFNO1lBQ3ZDLElBQU0sV0FBVyxHQUFHLE1BQU0sQ0FBQyxjQUFjLENBQUMsS0FBSyxFQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQ3BELEtBQUksQ0FBQyxHQUFHLENBQUMsT0FBTyxHQUFHLHlCQUF5QixHQUFDLFdBQVcsQ0FBQztZQUN6RCxLQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsU0FBUyxDQUNsQyxjQUFNLE9BQUEsT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsRUFBcEIsQ0FBb0IsRUFDMUIsVUFBQSxLQUFLLElBQUksT0FBQSxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxFQUFsQixDQUFrQixDQUM1QixDQUFDO1FBQ0osQ0FBQyxDQUFDLENBQUM7SUFFUCxDQUFDO0lBRUQscUNBQVcsR0FBWDtRQUFBLGlCQVlDO1FBWEMsSUFBSSxPQUFPLEdBQUc7WUFDWixLQUFLLEVBQUUsR0FBRztZQUNWLE1BQU0sRUFBRSxHQUFHO1lBQ1gsZUFBZSxFQUFFLElBQUk7WUFDckIsYUFBYSxFQUFFLElBQUk7U0FDcEIsQ0FBQztRQUNGLHdDQUFrQixFQUFFLENBQUM7UUFDckIsaUNBQVcsQ0FBQyxPQUFPLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxLQUFLO1lBQzdCLEtBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO1FBRXJCLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFBLEtBQUssSUFBSSxPQUFBLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLEVBQWxCLENBQWtCLENBQUMsQ0FBQTtJQUN2QyxDQUFDO0lBRUQscUNBQVcsR0FBWDtRQUFBLGlCQVNDO1FBUkMsSUFBSSxNQUFNLEdBQUcsV0FBVyxDQUFDLE1BQU0sQ0FBQztZQUM5QixJQUFJLEVBQUUsUUFBUTtTQUNmLENBQUMsQ0FBQztRQUVILE1BQU0sQ0FBQyxTQUFTLEVBQUU7YUFDakIsSUFBSSxDQUFDLGNBQU0sT0FBQSxNQUFNLENBQUMsT0FBTyxFQUFFLEVBQWhCLENBQWdCLENBQUM7YUFDNUIsSUFBSSxDQUFDLFVBQUMsS0FBSyxJQUFLLE9BQUEsS0FBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDLEVBQXJCLENBQXFCLENBQUM7YUFDdEMsS0FBSyxDQUFDLFVBQUMsS0FBSyxJQUFLLE9BQUEsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsRUFBbEIsQ0FBa0IsQ0FBQyxDQUFDO0lBQ3hDLENBQUM7SUE3Q1UsZUFBZTtRQU4zQixnQkFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLFlBQVk7WUFDdEIsV0FBVyxFQUFFLDBCQUEwQjtZQUN2QyxTQUFTLEVBQUUsQ0FBQyx5QkFBeUIsQ0FBQztZQUN0QyxRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7U0FDcEIsQ0FBQzt5Q0FLNkIsd0JBQVU7T0FKNUIsZUFBZSxDQStDM0I7SUFBRCxzQkFBQztDQUFBLEFBL0NELElBK0NDO0FBL0NZLDBDQUFlIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IERvZ1NlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlL2RvZy5zZXJ2aWNlJztcbmltcG9ydCB7IERvZyB9IGZyb20gJy4uL2VudGl0eS9kb2cnO1xuaW1wb3J0IHsgSW1hZ2VBc3NldCB9IGZyb20gXCJ0bnMtY29yZS1tb2R1bGVzL2ltYWdlLWFzc2V0XCI7XG5pbXBvcnQgeyBJbWFnZVNvdXJjZSB9IGZyb20gXCJ0bnMtY29yZS1tb2R1bGVzL2ltYWdlLXNvdXJjZVwiO1xuaW1wb3J0IHsgdGFrZVBpY3R1cmUsIHJlcXVlc3RQZXJtaXNzaW9ucyB9IGZyb20gXCJuYXRpdmVzY3JpcHQtY2FtZXJhXCI7XG5cbmltcG9ydCAqIGFzIGltYWdlcGlja2VyIGZyb20gXCJuYXRpdmVzY3JpcHQtaW1hZ2VwaWNrZXJcIjtcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnbnMtYWRkLWRvZycsXG4gIHRlbXBsYXRlVXJsOiAnLi9hZGQtZG9nLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vYWRkLWRvZy5jb21wb25lbnQuY3NzJ10sXG4gIG1vZHVsZUlkOiBtb2R1bGUuaWQsXG59KVxuZXhwb3J0IGNsYXNzIEFkZERvZ0NvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gIGRvZzogRG9nID0geyBuYW1lOiAnJywgYnJlZWQ6ICcnIH07XG4gIGltYWdlOiBJbWFnZUFzc2V0O1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgZG9nU2VydjogRG9nU2VydmljZSkgeyB9XG5cbiAgbmdPbkluaXQoKSB7XG4gIH1cblxuICBhZGQoKSB7XG4gICAgbGV0IHNvdXJjZSA9IG5ldyBJbWFnZVNvdXJjZSgpO1xuICAgICAgc291cmNlLmZyb21Bc3NldCh0aGlzLmltYWdlKS50aGVuKChzb3VyY2UpID0+IHsgXG4gICAgICAgIGNvbnN0IGJhc2U2NGltYWdlID0gc291cmNlLnRvQmFzZTY0U3RyaW5nKFwianBnXCIsNzApO1xuICAgICAgICB0aGlzLmRvZy5waWN0dXJlID0gJ2RhdGE6aW1hZ2UvanBlZztiYXNlNjQsJytiYXNlNjRpbWFnZTtcbiAgICAgICAgdGhpcy5kb2dTZXJ2LmFkZCh0aGlzLmRvZykuc3Vic2NyaWJlKFxuICAgICAgICAgICgpID0+IGNvbnNvbGUubG9nKCdicmF2bycpLFxuICAgICAgICAgIGVycm9yID0+IGNvbnNvbGUubG9nKGVycm9yKVxuICAgICAgICApO1xuICAgICAgfSk7XG4gICAgXG4gIH1cblxuICB0YWtlUGljdHVyZSgpIHtcbiAgICBsZXQgb3B0aW9ucyA9IHtcbiAgICAgIHdpZHRoOiAzMDAsXG4gICAgICBoZWlnaHQ6IDQwMCxcbiAgICAgIGtlZXBBc3BlY3RSYXRpbzogdHJ1ZSxcbiAgICAgIHNhdmVUb0dhbGxlcnk6IHRydWVcbiAgICB9O1xuICAgIHJlcXVlc3RQZXJtaXNzaW9ucygpO1xuICAgIHRha2VQaWN0dXJlKG9wdGlvbnMpLnRoZW4odmFsdWUgPT4ge1xuICAgICAgdGhpcy5pbWFnZSA9IHZhbHVlO1xuXG4gICAgfSkuY2F0Y2goZXJyb3IgPT4gY29uc29sZS5sb2coZXJyb3IpKVxuICB9XG5cbiAgcGlja1BpY3R1cmUoKSB7XG4gICAgbGV0IHBpY2tlciA9IGltYWdlcGlja2VyLmNyZWF0ZSh7XG4gICAgICBtb2RlOiAnc2luZ2xlJ1xuICAgIH0pO1xuICAgIFxuICAgIHBpY2tlci5hdXRob3JpemUoKVxuICAgIC50aGVuKCgpID0+IHBpY2tlci5wcmVzZW50KCkpXG4gICAgLnRoZW4oKGltYWdlKSA9PiB0aGlzLmltYWdlID0gaW1hZ2VbMF0pXG4gICAgLmNhdGNoKChlcnJvcikgPT4gY29uc29sZS5sb2coZXJyb3IpKTtcbiAgfVxuXG59XG4iXX0=