import { Component, OnInit } from '@angular/core';
import { DogService } from '../service/dog.service';
import { Dog } from '../entity/dog';
import { ImageAsset } from "tns-core-modules/image-asset";
import { ImageSource } from "tns-core-modules/image-source";
import { takePicture, requestPermissions } from "nativescript-camera";

import * as imagepicker from "nativescript-imagepicker";

@Component({
  selector: 'ns-add-dog',
  templateUrl: './add-dog.component.html',
  styleUrls: ['./add-dog.component.css'],
  moduleId: module.id,
})
export class AddDogComponent implements OnInit {
  dog: Dog = { name: '', breed: '' };
  image: ImageAsset;

  constructor(private dogServ: DogService) { }

  ngOnInit() {
  }

  add() {
    let source = new ImageSource();
      source.fromAsset(this.image).then((source) => { 
        const base64image = source.toBase64String("jpg",70);
        this.dog.picture = 'data:image/jpeg;base64,'+base64image;
        this.dogServ.add(this.dog).subscribe(
          () => console.log('bravo'),
          error => console.log(error)
        );
      });
    
  }

  takePicture() {
    let options = {
      width: 300,
      height: 400,
      keepAspectRatio: true,
      saveToGallery: true
    };
    requestPermissions()
    .then(() => takePicture(options))
    .then(value => {
      this.image = value;

    }).catch(error => console.log(error))
  }

  pickPicture() {
    let picker = imagepicker.create({
      mode: 'single'
    });
    
    picker.authorize()
    .then(() => picker.present())
    .then((image) => this.image = image[0])
    .catch((error) => console.log(error));
  }

}
