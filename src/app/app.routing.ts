import { NgModule } from "@angular/core";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { Routes } from "@angular/router";

import { ItemsComponent } from "./item/items.component";
import { ItemDetailComponent } from "./item/item-detail.component";
import { ExoLayoutComponent } from "./exo-layout/exo-layout.component";
import { LoginComponent } from "./login/login.component";
import { MyDogsComponent } from "./my-dogs/my-dogs.component";
import { SingleDogComponent } from "./single-dog/single-dog.component";
import { AddDogComponent } from "./add-dog/add-dog.component";

const routes: Routes = [
    { path: "", redirectTo: "/exo-layout", pathMatch: "full" },
    { path: "items", component: ItemsComponent },
    { path: "item/:id", component: ItemDetailComponent },
    { path: "exo-layout", component: ExoLayoutComponent },
    { path: "login", component: LoginComponent},
    { path: "my-dogs", component: MyDogsComponent},
    { path: "dog/:id", component: SingleDogComponent },
    { path: "add-dog", component:AddDogComponent}
];

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes)],
    exports: [NativeScriptRouterModule]
})
export class AppRoutingModule { }