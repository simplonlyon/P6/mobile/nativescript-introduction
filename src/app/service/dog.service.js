"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/common/http");
var environment_1 = require("../../environments/environment");
var DogService = /** @class */ (function () {
    function DogService(http) {
        this.http = http;
        this.apiUrl = environment_1.environment.serverUrl + '/api/dog/';
    }
    DogService.prototype.findAll = function () {
        return this.http.get(this.apiUrl);
    };
    DogService.prototype.find = function (id) {
        return this.http.get(this.apiUrl + 'single/' + id);
    };
    DogService.prototype.findByUser = function () {
        return this.http.get(this.apiUrl + 'user');
    };
    DogService.prototype.add = function (dog) {
        return this.http.post(this.apiUrl, dog);
    };
    DogService = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [http_1.HttpClient])
    ], DogService);
    return DogService;
}());
exports.DogService = DogService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZG9nLnNlcnZpY2UuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJkb2cuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUEyQztBQUMzQyw2Q0FBa0Q7QUFHbEQsOERBQTJEO0FBSzNEO0lBR0Usb0JBQW9CLElBQWU7UUFBZixTQUFJLEdBQUosSUFBSSxDQUFXO1FBRjNCLFdBQU0sR0FBRyx5QkFBVyxDQUFDLFNBQVMsR0FBQyxXQUFXLENBQUM7SUFFWixDQUFDO0lBRXhDLDRCQUFPLEdBQVA7UUFDRSxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQVEsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQzNDLENBQUM7SUFFRCx5QkFBSSxHQUFKLFVBQUssRUFBUztRQUNaLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBTSxJQUFJLENBQUMsTUFBTSxHQUFDLFNBQVMsR0FBQyxFQUFFLENBQUMsQ0FBQztJQUN0RCxDQUFDO0lBR0QsK0JBQVUsR0FBVjtRQUNFLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBUSxJQUFJLENBQUMsTUFBTSxHQUFDLE1BQU0sQ0FBQyxDQUFDO0lBRWxELENBQUM7SUFFRCx3QkFBRyxHQUFILFVBQUksR0FBTztRQUNULE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBTSxJQUFJLENBQUMsTUFBTSxFQUFFLEdBQUcsQ0FBQyxDQUFDO0lBQy9DLENBQUM7SUFyQlUsVUFBVTtRQUh0QixpQkFBVSxDQUFDO1lBQ1YsVUFBVSxFQUFFLE1BQU07U0FDbkIsQ0FBQzt5Q0FJeUIsaUJBQVU7T0FIeEIsVUFBVSxDQXNCdEI7SUFBRCxpQkFBQztDQUFBLEFBdEJELElBc0JDO0FBdEJZLGdDQUFVIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgSHR0cENsaWVudCB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcbmltcG9ydCB7IERvZyB9IGZyb20gJy4uL2VudGl0eS9kb2cnO1xuaW1wb3J0IHtlbnZpcm9ubWVudH0gZnJvbSAnLi4vLi4vZW52aXJvbm1lbnRzL2Vudmlyb25tZW50JztcblxuQEluamVjdGFibGUoe1xuICBwcm92aWRlZEluOiAncm9vdCdcbn0pXG5leHBvcnQgY2xhc3MgRG9nU2VydmljZSB7XG4gIHByaXZhdGUgYXBpVXJsID0gZW52aXJvbm1lbnQuc2VydmVyVXJsKycvYXBpL2RvZy8nO1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgaHR0cDpIdHRwQ2xpZW50KSB7IH1cblxuICBmaW5kQWxsKCk6IE9ic2VydmFibGU8RG9nW10+IHtcbiAgICByZXR1cm4gdGhpcy5odHRwLmdldDxEb2dbXT4odGhpcy5hcGlVcmwpO1xuICB9XG5cbiAgZmluZChpZDpudW1iZXIpOiBPYnNlcnZhYmxlPERvZz4ge1xuICAgIHJldHVybiB0aGlzLmh0dHAuZ2V0PERvZz4odGhpcy5hcGlVcmwrJ3NpbmdsZS8nK2lkKTtcbiAgfVxuXG4gIFxuICBmaW5kQnlVc2VyKCk6IE9ic2VydmFibGU8RG9nW10+IHtcbiAgICByZXR1cm4gdGhpcy5odHRwLmdldDxEb2dbXT4odGhpcy5hcGlVcmwrJ3VzZXInKTtcblxuICB9XG5cbiAgYWRkKGRvZzpEb2cpOiBPYnNlcnZhYmxlPERvZz4ge1xuICAgIHJldHVybiB0aGlzLmh0dHAucG9zdDxEb2c+KHRoaXMuYXBpVXJsLCBkb2cpO1xuICB9XG59XG4iXX0=