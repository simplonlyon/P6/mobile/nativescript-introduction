import { Component, OnInit } from '@angular/core';
import { DogService } from '../service/dog.service';
import { Dog } from '../entity/dog';

@Component({
  selector: 'ns-my-dogs',
  templateUrl: './my-dogs.component.html',
  styleUrls: ['./my-dogs.component.css'],
  moduleId: module.id,
})
export class MyDogsComponent implements OnInit {
  dogs:Dog[] = [];

  constructor(private dogServ:DogService) { }

  ngOnInit() {
    this.dogServ.findByUser().subscribe(
      data => this.dogs = data,
      error => console.log(error)
    );
  }

}
