"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var dog_service_1 = require("../service/dog.service");
var MyDogsComponent = /** @class */ (function () {
    function MyDogsComponent(dogServ) {
        this.dogServ = dogServ;
        this.dogs = [];
    }
    MyDogsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.dogServ.findByUser().subscribe(function (data) { return _this.dogs = data; }, function (error) { return console.log(error); });
    };
    MyDogsComponent = __decorate([
        core_1.Component({
            selector: 'ns-my-dogs',
            templateUrl: './my-dogs.component.html',
            styleUrls: ['./my-dogs.component.css'],
            moduleId: module.id,
        }),
        __metadata("design:paramtypes", [dog_service_1.DogService])
    ], MyDogsComponent);
    return MyDogsComponent;
}());
exports.MyDogsComponent = MyDogsComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXktZG9ncy5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJteS1kb2dzLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUFrRDtBQUNsRCxzREFBb0Q7QUFTcEQ7SUFHRSx5QkFBb0IsT0FBa0I7UUFBbEIsWUFBTyxHQUFQLE9BQU8sQ0FBVztRQUZ0QyxTQUFJLEdBQVMsRUFBRSxDQUFDO0lBRTBCLENBQUM7SUFFM0Msa0NBQVEsR0FBUjtRQUFBLGlCQUtDO1FBSkMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxVQUFVLEVBQUUsQ0FBQyxTQUFTLENBQ2pDLFVBQUEsSUFBSSxJQUFJLE9BQUEsS0FBSSxDQUFDLElBQUksR0FBRyxJQUFJLEVBQWhCLENBQWdCLEVBQ3hCLFVBQUEsS0FBSyxJQUFJLE9BQUEsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsRUFBbEIsQ0FBa0IsQ0FDNUIsQ0FBQztJQUNKLENBQUM7SUFWVSxlQUFlO1FBTjNCLGdCQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsWUFBWTtZQUN0QixXQUFXLEVBQUUsMEJBQTBCO1lBQ3ZDLFNBQVMsRUFBRSxDQUFDLHlCQUF5QixDQUFDO1lBQ3RDLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtTQUNwQixDQUFDO3lDQUk0Qix3QkFBVTtPQUgzQixlQUFlLENBWTNCO0lBQUQsc0JBQUM7Q0FBQSxBQVpELElBWUM7QUFaWSwwQ0FBZSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBEb2dTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZS9kb2cuc2VydmljZSc7XG5pbXBvcnQgeyBEb2cgfSBmcm9tICcuLi9lbnRpdHkvZG9nJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnbnMtbXktZG9ncycsXG4gIHRlbXBsYXRlVXJsOiAnLi9teS1kb2dzLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vbXktZG9ncy5jb21wb25lbnQuY3NzJ10sXG4gIG1vZHVsZUlkOiBtb2R1bGUuaWQsXG59KVxuZXhwb3J0IGNsYXNzIE15RG9nc0NvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gIGRvZ3M6RG9nW10gPSBbXTtcblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIGRvZ1NlcnY6RG9nU2VydmljZSkgeyB9XG5cbiAgbmdPbkluaXQoKSB7XG4gICAgdGhpcy5kb2dTZXJ2LmZpbmRCeVVzZXIoKS5zdWJzY3JpYmUoXG4gICAgICBkYXRhID0+IHRoaXMuZG9ncyA9IGRhdGEsXG4gICAgICBlcnJvciA9PiBjb25zb2xlLmxvZyhlcnJvcilcbiAgICApO1xuICB9XG5cbn1cbiJdfQ==