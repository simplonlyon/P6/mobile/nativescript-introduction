"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var dog_service_1 = require("../service/dog.service");
var operators_1 = require("rxjs/operators");
var environment_1 = require("~/environments/environment");
var SingleDogComponent = /** @class */ (function () {
    function SingleDogComponent(pageRoute, dogServ) {
        this.pageRoute = pageRoute;
        this.dogServ = dogServ;
    }
    SingleDogComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.pageRoute.activatedRoute.pipe(operators_1.switchMap(function (activatedRoute) { return activatedRoute.params; }), operators_1.switchMap(function (params) { return _this.dogServ.find(params['id']); })).subscribe(function (dog) { return _this.dog = dog; }, function (error) { return console.log(error); });
    };
    Object.defineProperty(SingleDogComponent.prototype, "dogPicture", {
        get: function () {
            return environment_1.environment.serverUrl + '/' + this.dog.picture;
        },
        enumerable: true,
        configurable: true
    });
    SingleDogComponent = __decorate([
        core_1.Component({
            selector: 'ns-single-dog',
            templateUrl: './single-dog.component.html',
            styleUrls: ['./single-dog.component.css'],
            moduleId: module.id,
        }),
        __metadata("design:paramtypes", [router_1.PageRoute, dog_service_1.DogService])
    ], SingleDogComponent);
    return SingleDogComponent;
}());
exports.SingleDogComponent = SingleDogComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2luZ2xlLWRvZy5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJzaW5nbGUtZG9nLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUFrRDtBQUNsRCxzREFBd0Q7QUFDeEQsc0RBQW9EO0FBRXBELDRDQUEyQztBQUUzQywwREFBeUQ7QUFRekQ7SUFFRSw0QkFBb0IsU0FBbUIsRUFBVSxPQUFrQjtRQUEvQyxjQUFTLEdBQVQsU0FBUyxDQUFVO1FBQVUsWUFBTyxHQUFQLE9BQU8sQ0FBVztJQUFJLENBQUM7SUFFeEUscUNBQVEsR0FBUjtRQUFBLGlCQVFDO1FBUEMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUNoQyxxQkFBUyxDQUFDLFVBQUEsY0FBYyxJQUFJLE9BQUEsY0FBYyxDQUFDLE1BQU0sRUFBckIsQ0FBcUIsQ0FBQyxFQUNsRCxxQkFBUyxDQUFDLFVBQUEsTUFBTSxJQUFJLE9BQUEsS0FBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQS9CLENBQStCLENBQUMsQ0FDckQsQ0FBQyxTQUFTLENBQ1QsVUFBQSxHQUFHLElBQUksT0FBQSxLQUFJLENBQUMsR0FBRyxHQUFHLEdBQUcsRUFBZCxDQUFjLEVBQ3JCLFVBQUEsS0FBSyxJQUFJLE9BQUEsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsRUFBbEIsQ0FBa0IsQ0FDNUIsQ0FBQTtJQUNILENBQUM7SUFFRCxzQkFBSSwwQ0FBVTthQUFkO1lBQ0UsTUFBTSxDQUFDLHlCQUFXLENBQUMsU0FBUyxHQUFDLEdBQUcsR0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQztRQUNwRCxDQUFDOzs7T0FBQTtJQWhCVSxrQkFBa0I7UUFOOUIsZ0JBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxlQUFlO1lBQ3pCLFdBQVcsRUFBRSw2QkFBNkI7WUFDMUMsU0FBUyxFQUFFLENBQUMsNEJBQTRCLENBQUM7WUFDekMsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1NBQ3BCLENBQUM7eUNBRzhCLGtCQUFTLEVBQWtCLHdCQUFVO09BRnhELGtCQUFrQixDQWtCOUI7SUFBRCx5QkFBQztDQUFBLEFBbEJELElBa0JDO0FBbEJZLGdEQUFrQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBQYWdlUm91dGUgfSBmcm9tICduYXRpdmVzY3JpcHQtYW5ndWxhci9yb3V0ZXInO1xuaW1wb3J0IHsgRG9nU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2UvZG9nLnNlcnZpY2UnO1xuaW1wb3J0IHsgRG9nIH0gZnJvbSAnLi4vZW50aXR5L2RvZyc7XG5pbXBvcnQgeyBzd2l0Y2hNYXAgfSBmcm9tIFwicnhqcy9vcGVyYXRvcnNcIjtcbmltcG9ydCB7IEFjdGl2YXRlZFJvdXRlIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcbmltcG9ydCB7IGVudmlyb25tZW50IH0gZnJvbSAnfi9lbnZpcm9ubWVudHMvZW52aXJvbm1lbnQnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICducy1zaW5nbGUtZG9nJyxcbiAgdGVtcGxhdGVVcmw6ICcuL3NpbmdsZS1kb2cuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9zaW5nbGUtZG9nLmNvbXBvbmVudC5jc3MnXSxcbiAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcbn0pXG5leHBvcnQgY2xhc3MgU2luZ2xlRG9nQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgZG9nOkRvZztcbiAgY29uc3RydWN0b3IocHJpdmF0ZSBwYWdlUm91dGU6UGFnZVJvdXRlLCBwcml2YXRlIGRvZ1NlcnY6RG9nU2VydmljZSkgeyB9XG5cbiAgbmdPbkluaXQoKSB7XG4gICAgdGhpcy5wYWdlUm91dGUuYWN0aXZhdGVkUm91dGUucGlwZShcbiAgICAgIHN3aXRjaE1hcChhY3RpdmF0ZWRSb3V0ZSA9PiBhY3RpdmF0ZWRSb3V0ZS5wYXJhbXMpLFxuICAgICAgc3dpdGNoTWFwKHBhcmFtcyA9PiB0aGlzLmRvZ1NlcnYuZmluZChwYXJhbXNbJ2lkJ10pKVxuICAgICkuc3Vic2NyaWJlKFxuICAgICAgZG9nID0+IHRoaXMuZG9nID0gZG9nLFxuICAgICAgZXJyb3IgPT4gY29uc29sZS5sb2coZXJyb3IpXG4gICAgKVxuICB9XG5cbiAgZ2V0IGRvZ1BpY3R1cmUoKSB7XG4gICAgcmV0dXJuIGVudmlyb25tZW50LnNlcnZlclVybCsnLycrdGhpcy5kb2cucGljdHVyZTtcbiAgfVxuXG59XG4iXX0=