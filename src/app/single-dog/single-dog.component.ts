import { Component, OnInit } from '@angular/core';
import { PageRoute } from 'nativescript-angular/router';
import { DogService } from '../service/dog.service';
import { Dog } from '../entity/dog';
import { switchMap } from "rxjs/operators";
import { ActivatedRoute } from '@angular/router';
import { environment } from '~/environments/environment';

@Component({
  selector: 'ns-single-dog',
  templateUrl: './single-dog.component.html',
  styleUrls: ['./single-dog.component.css'],
  moduleId: module.id,
})
export class SingleDogComponent implements OnInit {
  dog:Dog;
  constructor(private pageRoute:PageRoute, private dogServ:DogService) { }

  ngOnInit() {
    this.pageRoute.activatedRoute.pipe(
      switchMap(activatedRoute => activatedRoute.params),
      switchMap(params => this.dogServ.find(params['id']))
    ).subscribe(
      dog => this.dog = dog,
      error => console.log(error)
    )
  }

  get dogPicture() {
    return environment.serverUrl+'/'+this.dog.picture;
  }

}
