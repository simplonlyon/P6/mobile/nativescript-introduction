export interface Dog {
    id?:number;
    name:string;
    breed:string;
    picture?:any;
    user?:any
}