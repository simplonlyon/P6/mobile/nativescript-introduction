
/**
 * L'ip en 10.0.2.2 pointe sur le localhost de la machine depuis
 * un émulateur. L'émulateur n'a pas accès directement à localhost
 */
export const environment = {
  production: false,
  serverUrl: 'http://10.0.2.2:8080'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
